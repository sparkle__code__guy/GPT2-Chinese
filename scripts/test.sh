#!/usr/bin/env bash
python generate.py \
  --device 0 \
  --length 100 \
  --nsamples  4 \
  --model_config ./gpt2/config.json \
  --tokenizer_path ./gpt2/vocab.txt \
  --model_path ./gpt2 \
  --prefix "[CLS][MASK]" \
  --topp 1 \
  --temperature 1.0